#!/bin/bash

# Script to build android linux distribiution with some customisations for Samsuns S7 device

# Exit on first failure
set -e

# Show each command
set -x

# Go to this script directory
cd $(dirname $(realpath "$0"))

# Check if android sources exist already
if ! [ -d android ]; then

    # Create directory with android sources and build output
    mkdir android

    # Run in subshell to preserve working directory in current shell
    (

    	# Go to sources directory
    	cd android

	# Initialize repo
	yes | repo init -u https://github.com/LineageOS/android.git -b cm-14.1

	# Download andoird sources repository
	repo sync

        # Download device specific closed binaries
        git clone \
            `# Download only last commit` \
            --depth 1 \
            \
            `# Branch to use` \
            --branch lineage-16.0 \
            \
            `# Source repo url` \
            https://github.com/TheMuppets/proprietary_vendor_samsung.git \
            \
            `# Target directory` \
            vendor/samsung

	# Do not exit on any failure and do not show each command
	set +xe

	# Prepare environement
	source build/envsetup.sh

	# Download device specific source files
	breakfast herolte
    )
fi

# Go to sources directory
cd android

# Do not exit on any failure and do not show each command
set +xe

# Prepare environement
source build/envsetup.sh

# Build image
LC_ALL=C \
ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx10G" \
WITH_SU=true \
brunch herolte
