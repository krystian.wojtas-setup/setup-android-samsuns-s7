#!/bin/sh

# Stop on first failure
set -e

# Show each command
set -x

# Go to this script directory
cd "$(realpath $(dirname "$0"))"

# Build docker image
docker build \
     `# Image is based on openjdk` \
     --tag openjdk:8-lineageos \
     \
     `# Current build directory` \
     .
