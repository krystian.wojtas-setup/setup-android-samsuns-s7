#!/bin/sh

# Script is to run prepared previously docker image
# Command to run in docker container could be passed as script parameter
# or by default it builds android image

# Show each command
set -x

# Command to run in docker container could be passed as script arguments
command="$@"

# If command to run is not passed, then run build.sh script as default one
# In case of local build script build.sh should be provided with this repository by mounted volume
[ -z "$command" ] && set './build.sh'

# Go to this script directory
cd "$(realpath $(dirname "$0"))"

# Set work dir to parent of current one
WORKDIR="$(realpath "$(pwd)/../")"

# Run docker container
docker run \
    `# Set user and group id inside container to match host ones` \
    --env LOCAL_UID="$(id -u)" \
    --env LOCAL_GID="$(id -g)" \
    \
    `# Mount current repository from host into docker` \
    --volume "$WORKDIR:$WORKDIR" \
    \
    `# Set working directory inside container as this repository` \
    --workdir "$WORKDIR" \
    \
    `# Allow to work on container interacively` \
    --interactive \
    --tty \
    \
    `# Remove container after use` \
    --rm \
    \
    `# Extra docker flags if needed` \
    $EXTRA_DOCKER_FLAGS \
    \
    `# Docker image which have to be built firstly` \
    openjdk:8-lineageos \
    \
    `# Run command` \
    "$@"
