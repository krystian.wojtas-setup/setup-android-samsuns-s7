FROM openjdk:8

###################
# This Dockerfile was based on the following Dockerfiles
# - docker-lineageos: existing unoptimized image
#    https://github.com/AnthoDingo/docker-lineageos/blob/autobuild/Dockerfile
#

# Install packages
RUN set -ex ;\
    apt-get update && apt-get install -y --no-install-recommends \
          # install sdk
          # https://wiki.lineageos.org/devices/klte/build#install-the-build-packages
          android-sdk-platform-tools-common \
          # install packages
          # https://wiki.lineageos.org/devices/klte/build#install-the-build-packages
	  bc \
	  bison \
	  build-essential \
          curl \
	  flex \
	  g++-multilib \
	  gcc-multilib \
	  git \
	  gnupg \
	  gperf \
	  imagemagick \
	  lib32ncurses5-dev \
	  lib32readline-dev \
	  lib32z1-dev \
	  liblz4-tool \
	  libncurses5-dev \
	  libsdl1.2-dev \
	  libssl-dev \
	  libwxgtk3.0-dev \
	  libxml2 \
	  libxml2-utils \
	  lzop \
	  pngcrush \
	  rsync \
	  schedtool \
	  squashfs-tools \
	  xsltproc \
	  zip \
	  zlib1g-dev \
          \
          # extra packages
          # has 'col' package needed for 'breakfast'
          bsdmainutils \
          gosu \
          # for git-repo from google
          python \
          sudo \
          ;\
    rm -rf /var/lib/apt/lists/*

# Run config in a seperate layer so we cache it
RUN set -ex ;\
    # Install repo tool
    curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/bin/repo ;\
    chmod a+x /usr/bin/repo ;

# Copy entrypoint into conatiner
COPY entrypoint.sh /usr/local/bin/

# Set entrypoint
ENTRYPOINT ["bash", "/usr/local/bin/entrypoint.sh"]

# Use bash
CMD ["/bin/bash"]
